<?php
/**
 * -----------------------------------------------------------------------------------------------------------
 *      Debug Helper Functions
 * -----------------------------------------------------------------------------------------------------------
 */

/* General Print Function */
if (!function_exists('pm'))
{
    function pm($var, $exit = 0, $new_line = 0)
    {
        echo '<pre>';
        print_r($var);
        echo '</pre>';

        if ($new_line)
        {
            echo '<br/>';
        }

        if ($exit)
        {
            exit();
        }

    }
}

/* General Print Function and Exit. */
if (!function_exists('pme'))
{
    function pme($var)
    {
        pm($var, 1);
    }
}

/* General Print Function for Terminal */
if (!function_exists('pc'))
{
    function pc($var, $exit = 0, $msgName = null, $new_line = 0)
    {
        //$var =
        echo "\n";
        if (!empty($msgName))
        {
            echo "$msgName: \n";}
        print_r($var);
        echo "\n";

        if ($new_line)
        {
            echo "\n\n";
        }

        if ($exit)
        {
            exit();
        }
    }
}

/* General Print Function for Terminal and Exit. */
if (!function_exists('pce'))
{
    function pce($var)
    {
        pc($var, 1);
    }
}

/* Print with json response */
if (!function_exists('pj'))
{
    function pj($var)
    {
        return response()->json($var);
    }
}

if (!function_exists('pqs'))
{
    function pqs()
    {
        DB::enableQueryLog(); // Enable the Laravel Database Query Log
    }
}

if (!function_exists('pq'))
{
    function pq()
    {
        pm(DB::getQueryLog()); // Fetch the Laravel Database Last Query Log
    }
}

if (!function_exists('php'))
{
    function php()
    {
        pce('Current PHP version: ' . phpversion());
    }
}
