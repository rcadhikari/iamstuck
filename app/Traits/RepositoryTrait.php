<?php
namespace App\Traits;

use Illuminate\Support\Facades\DB;

trait RepositoryTrait
{

    public function executeRawQuery($query)
    {
        return DB::select($query);
    }

}
