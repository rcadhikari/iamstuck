<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    //
    protected $table = "submissions";
    protected $fillable = [
        'firstname',
        'lastname',
        'gender_id',
        'mobile_phone',
        'email',
        'citizen_of',
        'permanent_address',
        'stuck_in_country',
        'city',
        'address',
        'reason_id',
        'alone_group',
        'members_above_or_16',
        'members_under_16',
        'explain_situation',
        'verify',
        'rescue_status'
    ];

    public function caseType()
    {
        return $this->belongsTo(Casetype::class, 'case_type_id');
    }

    public function citizenOf()
    {
        return $this->belongsTo(Country::class, 'citizen_of');
    }

    public function gender()
    {
        return $this->belongsTo(Gender::class, 'case_type_id');
    }

    public function reason()
    {
        return $this->belongsTo(Reason::class, 'reason_id');
    }

    public function stuckInCountry()
    {
        return $this->belongsTo(Country::class, 'stuck_in_country');
    }
}
