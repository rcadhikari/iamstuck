<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Casetype extends Model
{
    //
    protected $table = "case_types";

    public static function caseType()
    {
        return static::pluck('name', 'id');
    }
}
