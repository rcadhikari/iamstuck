<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{
    //
    protected $table = "genders";

    public static function gender()
    {
        return static::pluck('name', 'id');
    }
}
