<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reason extends Model
{
    //
    public static function reason()
    {
        return static::pluck('name', 'id');
    }
}
