<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rescuer extends Model
{
    //
    protected $table = "rescuers";

    protected $fillable = [
        'rescuer_type_id',
        'security_token',
        'user_id',
        'name',
        'email',
        'country_id',
        'city',
        'address',
        'description',
        'phone',
        'services',
        'status'
    ];

    public function rescuerType()
    {
        return $this->belongsTo(Rescuetype::class, 'rescuer_type_id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function rescues()
    {
        return $this->hasMany(Rescue::class, 'rescuer_id');
    }

}
