<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rescue extends Model
{

    protected $table = "rescues";
    protected $fillables = [
        'rescuer_id',
        'submission_id',
        'message',
        'is_helpful',
        'gratitude',
        'status'
    ];

    public function rescuer()
    {
        return $this->belongsTo(Rescuer::class, 'rescuer_id');
    }

    public function submission()
    {
        return $this->belongsTo(Submission::class, 'submission_id');
    }
}
