<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rescuetype extends Model
{
    //
    protected $table = "rescuer_types";


    public static function rescueType()
    {
        return static::pluck('name', 'id');
    }

}
