<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //

    protected $table = "countries";

    public static function country()
    {
        return static::pluck('name', 'id');
    }

    
}
