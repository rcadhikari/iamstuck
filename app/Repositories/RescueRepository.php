<?php

namespace App\Repositories;


use App\Models\Rescue;
use App\Models\Rescuer;
use App\Traits\RepositoryTrait;

class RescueRepository {
    use RepositoryTrait;

    public function __construct(
        Rescue $rescue,
        Rescuer $rescuer
    )
    {
        $this->rescue = $rescue;
        $this->rescuer = $rescuer;
    }

    public function getRescuesByRescuers(int $countryId)
    {
        return $this->rescuer
            ->with(['rescuerType', 'country'])
            ->where('country_id', $countryId)
            ->where('status', 1)
            ->get();
    }

    public function getRescuesSummary($rows)
    {
        return (object) [
            'total_rescuers' => count($rows),
        ];
    }

}
