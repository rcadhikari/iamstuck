<?php

namespace App\Repositories;


use App\Models\Submission;
use App\Traits\RepositoryTrait;

/**
 * @property Submission submission
 */
class ReportRepository {
    use RepositoryTrait;

    public function __construct(
        Submission $submission
    )
    {
        $this->submission = $submission;
    }

    public function getOverallStatistics()
    {
        $query = 'select
                        t1.total_countries_stranded,
                        t1.stranded_people,
                        t2.total_countries_rescued,
                        t2.rescued_people,
                        r1.total_rescuers,
	                    r2.total_rescuers_countries
                    from
                        (
                        select
                            count(s1.citizen_of) as total_countries_stranded,
                            sum(s1.members_under_16 + s1.members_above_or_16) as stranded_people
                        from
                            submissions s1
                        where
                            s1.verify = 1
                            AND s1.rescue_status < 2 ) as t1,
                        (
                        select
                            count(s2.citizen_of) as total_countries_rescued,
                            sum(s2.members_under_16 + s2.members_above_or_16) as rescued_people
                        from
                            submissions s2
                        where
                            s2.verify = 1
                            AND s2.rescue_status = 2 ) as t2, 
                        (
                        select
                            count(id) as total_rescuers
                        from
                            rescuers
                        where
                            status = 1) as r1,
                        (
                        select
                            count(DISTINCT country_id) as total_rescuers_countries
                        from
                            rescuers
                        where
                            status = 1) as r2';

        $result = $this->executeRawQuery($query);

        return $result[0] ?? null;
    }

    public function getSubmissionReport(int $caseTypeId)
    {
        $query = 'select
                        c.id,
                        c.nicename as country,
	                    c.iso as country_code,
                        n.nationals,
                        f.foreigners,
                        r.rescuers
                    from
                        countries c
                    left join (
                            select
                                s1.citizen_of as country_id_1,
                                c.nicename as country_1,
                                sum(s1.members_under_16 + s1.members_above_or_16) as nationals
                            from
                                submissions s1
                            left join countries c on
                                s1.citizen_of = c.id
                            where
                                s1.case_type_id = ' . $caseTypeId . '
                                AND s1.verify = 1
                                AND s1.rescue_status < 2
                            group by
                                s1.citizen_of 
                        ) as n on c.id = n.country_id_1
                    left join (
                            select
                                s2.stuck_in_country as country_id_2,
                                c2.nicename as country_2,
                                sum(s2.members_under_16 + s2.members_above_or_16) as foreigners
                            from
                                submissions s2
                            left join countries c2 on
                                s2.stuck_in_country = c2.id
                            where
                                s2.case_type_id = ' . $caseTypeId . '
                                AND s2.verify = 1
                                AND s2.rescue_status < 2
                            group by
                                s2.stuck_in_country 
                        ) as f on c.id = f.country_id_2
                    left join (
                            select
                                country_id,
                                count(country_id) as rescuers
                            from
                                rescuers
                            where
                                status = 1
                            group by country_id
                        ) as r on c.id = r.country_id
                    where 
                        n.nationals IS NOT NULL 
                        OR f.foreigners IS NOT NULL 
                        OR r.rescuers IS NOT NULL
                    order by
                        n.nationals desc,
                        f.foreigners desc,
                        r.rescuers desc,
                        c.nicename asc';

        return $this->executeRawQuery($query);
    }

    public function getSubmissionsForNationals(int $countryId, int $caseTypeId)
    {
        return $this->submission
            ->with(['caseType', 'citizenOf', 'gender', 'stuckInCountry'])
            ->where('citizen_of', $countryId)
            ->where('case_type_id', $caseTypeId)
            ->get();
    }

    public function getSubmissionsByForeigners(int $countryId, int $caseTypeId)
    {
        return $this->submission
            ->with(['caseType', 'citizenOf', 'gender', 'stuckInCountry'])
            ->where('stuck_in_country', $countryId)
            ->where('case_type_id', $caseTypeId)
            ->get();
    }

    public function getSubmissionsSummary($rows)
    {
        $count = 0;
        $countPeople = 0;
        foreach ($rows as $row) {
            $count++;
            $countPeople += $row->members_above_or_16 + $row->members_under_16;
        }

        return (object) [
            'total_submissions' => $count,
            'total_people' => $countPeople,
        ];
    }
}
