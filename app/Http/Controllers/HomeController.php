<?php

namespace App\Http\Controllers;

use App\Repositories\ReportRepository;
use Illuminate\Http\Request;

/**
 * @property ReportRepository reportRepository
 */
class HomeController extends Controller
{
    const CASE_TYPE_ID = 1; # Covid19, TODO: require to replace with dynamic.

    public function __construct(
        ReportRepository $reportRepository
    )
    {
        $this->reportRepository = $reportRepository;
    }

    public function home(Request $request)
    {
        $caseTypeId = $request->input('case_type_id', self::CASE_TYPE_ID);
        $total = $this->reportRepository->getOverallStatistics();
        $reports = $this->reportRepository->getSubmissionReport($caseTypeId);

        return view('home', [
            'total' => $total,
            'reports' => $reports
        ]);
    }

    public function about(){
        return view('about');
    }
}
