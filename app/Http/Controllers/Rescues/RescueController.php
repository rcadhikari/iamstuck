<?php

namespace App\Http\Controllers\Rescues;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Rescue;
use App\Models\Submission;
use App\Models\Rescuer;
use Illuminate\Support\Facades\DB;

class RescueController extends Controller
{

    public function store(Request $request)
    {
        $email = $request->email;
        $rescuer = $this->getRescuers($email);

        if(is_null($rescuer)) {

            //TODO: please advice to register as rescuer.

        }
        else{
        
            if($rescuer->security_token != $request->Pin){
               //TODO: Pin incorrect
            
            }

        else {

            $rescue = Rescue::create();
            $rescue->rescuer_id = $rescuer->id;
            $rescue->submission_id = $request->submission_id;
            $rescue->message = $request->message;
            $rescue->is_helpful = NULL;
            $rescue->gratitude = NULL;
            $rescue->status = NULL;
                            
            $rescue->save();

            $submission = Submission::findOrFail($request->submission_id);

            }
        }

        $msg = ["message" => "<h1>Thank you for your request to help '$submission->firstname'"];


        return redirect()->route('rescuer.submitted')->with($msg);


    }

    private function getRescuers($email)
    {
        return $rescuer = DB::table('rescuers')->where('email', $email)->first();
    }
    
}
