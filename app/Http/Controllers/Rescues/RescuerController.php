<?php

namespace App\Http\Controllers\Rescues;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\Rescuetype;
use App\Models\Rescuer;
class RescuerController extends Controller
{
    
    public function create()
    {
        //
       
  
        $country = Country::pluck('name', 'id')->prepend(trans('Please Select'), '');    
        $rescuer_type = Rescuetype::pluck('name', 'id')->prepend(trans('Please Select'), '');         
        return view('rescuers.create', compact('country',  'rescuer_type'));

    }

    
    public function store(Request $request)
    {
        //
        $length_of_string = 4;
        $str_result = '0123456789'; 
        //$security_token = substr(str_shuffle($str_result), 0, $length_of_string); 

        $rescuer = Rescuer::create($request->all());       
        $rescuer->security_token = substr(str_shuffle($str_result), 0, $length_of_string); 
        $rescuer->status = 1;        
        $rescuer->save();

        $msg = ["message" => "<h1>Thank you for your request to become a rescuer.</h1> <p>Please save these details you will need it when you want to help someone.</p> <br><br> Your Email : '$rescuer->email' <br> <br> Your Pin : '$rescuer->security_token'"];


        return redirect()->route('rescuer.submitted')->with($msg);

    }

    

    public function submitted(){

        //Print msg
        //TODO: print the code & message

        return view('rescuers.submitted');

    }
}
