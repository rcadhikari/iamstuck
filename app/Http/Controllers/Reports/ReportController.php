<?php

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use App\Repositories\ReportRepository;
use App\Repositories\RescueRepository;
use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\Gender;
use App\Models\Reason;
use App\Models\Submission;
use App\Models\Casetype;


class ReportController extends Controller
{
    const CASE_TYPE_ID = 1; # Covid19, TODO: require to replace with dynamic.

    /**
     * @var ReportRepository
     */
    private $reportRepository;

    /**
     * @var RescueRepository
     */
    private $rescueRepository;

    public function __construct(
        ReportRepository $reportRepository,
        RescueRepository $rescueRepository
    )
    {
        $this->reportRepository = $reportRepository;
        $this->rescueRepository = $rescueRepository;
    }

    public function create()
    {
        $country = Country::pluck('name', 'id')->prepend(trans('Please Select'), '');
        $gender = Gender::pluck('name', 'id')->prepend(trans('Please Select'), '');
        $reason = Reason::pluck('name', 'id')->prepend(trans('Please Select'), '');
        $case_type = Casetype::pluck('name', 'id');
        $numbers = array(
            '0'=>'0', '1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8',
            '9'=>'9','10'=>'10','11'=>'11','12'=>'12','13'=>'13','14'=>'14','15'=>'15','16'=>'16',
            '17'=>'17','18'=>'18','19'=>'19','20'=>'20'
        );

        return view('submissions.create', compact('country', 'gender', 'reason', 'case_type', 'numbers'));
    }

    public function store(Request $request)
    {
        $submission = Submission::create($request->all());
        $submission->save();
            
       //Create user
       //send email
       //update the City table

        $msg = ["message" => "You submission have been posted"];

        return redirect()->route('report.submitted')->with($msg);
    }

    public function submitted()
    {
        return view('submissions.submitted');
    }

    public function show(int $countryId, Request $request)
    {
        $caseTypeId = $request->input('case_type_id', self::CASE_TYPE_ID);
        $info = Country::find($countryId);
        $nationals = $this->reportRepository->getSubmissionsForNationals($countryId, $caseTypeId);
        $foreigners = $this->reportRepository->getSubmissionsByForeigners($countryId, $caseTypeId);
        $rescuers = $this->rescueRepository->getRescuesByRescuers($countryId);

        return view('reports.detail', [
            'info' => $info,
            'nationals' => [
                'summary' => $this->reportRepository->getSubmissionsSummary($nationals),
                'reports' => $nationals,
            ],
            'foreigners' => [
                'summary' => $this->reportRepository->getSubmissionsSummary($foreigners),
                'reports' => $foreigners,
            ],
            'rescuers' => [
                'summary' => $this->rescueRepository->getRescuesSummary($rescuers),
                'reports' => $rescuers,
            ],
        ]);
    }
}
