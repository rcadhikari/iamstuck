@extends('base.layout')


@section('content')

<div class="alert alert-info" role="alert">
<div class="container bg-light-blue floating">
    <div class="row">
        <div class="col-12 px-3 py-5 p-sm-4 px-lg-5 py-lg-4">
            <h1 class="font-w700 text-body">About</h1>
            <hr>
            <p>Due to the pandemic, there has been a global crisis and many countries have locked down borders and flights.<br> During this time, many people are unable to reach their home and they have now stuck in a place away from their home. 
            <br> Many of them are in a difficult situation and to help these people at this time we need collective support.<br> Therefore, we came up with an idea where we can connect everyone who is willing to contribute and the one who is in need.</p>
             <hr>               
             <h3 class="font-w700 text-body">What it does</h3>
             <ul>
                 <li>Allow individual or group representative to submit basic information (Name, Phone, Email, Stuck City/Address, and their destination address) and their current situation.</li>
                 <li>It provides a report of stranded people to concerned stakeholders such as countries representative, embassies, social workers, NGOs/INGOs, Media, and any individual.</li>
             </ul>
             <hr>
             <h3 class="font-w700 text-body">Next Steps</h3>
            <p> For the next phase, we would like to include the following: </p>
            <ul>
                <li>Provide membership to reporter/stranded individuals</li>
                <li>Add notification via direct app and SMS for prompt responses</li>
                <li>Improve responsive design for mobile devices</li>
                <li>Improve the stranded workflow to follow up with individuals until they are fully rescued</li>
                <li>Add features for volunteers to offer more support to the local area and region level with more personalized data</li>
                <li>Gather much wider data and reach out to global audiences.</li>
            </ul>
             <hr>
             <h3 class="font-w700 text-body">Contact us</h3>
             <p>
                 If you want to contribute or be part of our journey or for a query, <br/>
                 please us email at <a href="iamstuck.info@gmail.com">iamstuck.info@gmail.com</a>.
             </p>
            </div>
        </div>
    </div>
</div>
</div>





@endsection
