<div class="form-group required col-12">
    <label class="control-label" for="message">Message</label>
     {!!Form::textarea('message', old('message'), ['class' => 'form-control', 'placeholder' => '', 'required' => 'true']) !!}
    <p class="help-block"></p>
    @if($errors->has('message'))
        <p class="help-block alert alert-danger">
            {{ $errors->first('message') }}
        </p>
    @endif

</div>

<div class="form-group required col-12">
    <label class="control-label" for="email">Email address</label>
    {!! Form::text('email', old('email'), ['class' => 'form-control', 'placeholder' => '', 'required' => 'true']) !!}
    <p class="help-block"></p>
    @if($errors->has('email'))
        <p class="help-block alert alert-danger">
            {!! $errors->first('email') !!}
        </p>
    @endif
</div>

<div class="form-group required col-12">
	<label class="control-label" for="Pin">Personal Pin</label>
	{!! Form::text('Pin', old('Pin'), ['class' => 'form-control', 'placeholder' => '', 'required' => 'true']) !!}
	<p class="help-block"></p>
	@if($errors->has('Pin'))
		<p class="help-block alert alert-danger">
			{!! $errors->first('Pin') !!}
		</p>
	@endif
 </div>