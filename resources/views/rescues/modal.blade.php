<!-- Modal -->
<div class="modal fade" id="m{{$row->id}}" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
		<h5 class="modal-title">Offer Help</h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
        <div class="modal-body">
          @include('rescuers.rescue')
        </div>
        
      </div>
      
    </div>
  </div>
 <!-- Modal content-->