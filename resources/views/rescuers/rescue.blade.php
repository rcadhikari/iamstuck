{!! Form::open(['method' => 'POST', 'route' => ['save-rescue'], 'files' => true,]) !!}

@include('rescues.form')
<input type="hidden" id="submission_id" name="submission_id" value="{{$row->id}}">

        <div class="modal-footer">
            {!! Form::submit(trans('Submit'), ['class' => 'btn btn-lg btn-success']) !!}    
        </div>
  
   
 
{!! Form::close() !!}