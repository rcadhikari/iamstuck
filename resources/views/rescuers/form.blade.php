<div class="row mt-2">
	<div class="col-12">
		<div class="shadow p-3 mb-5 bg-white rounded">
			<div class="row">
			<div class="col-12 text-center">
			</div>
				<div class="form-group required col-md-6">
					<label class="control-label" for="rescuer_type_id">Category</label>
					{!! Form::select('rescuer_type_id', $rescuer_type,  old('rescuer_type_id'), ['class' => 'form-control', 'required' => 'true']) !!}
						<p class="help-block"></p>
					@if($errors->has('rescuer_type_id'))
						<p class="help-block alert alert-danger">
							{{ $errors->first('rescuer_type_id') }}
						</p>
					@endif

				</div>

				<div class="form-group required col-md-6">
					<label class="control-label" for="name">Name</label>
					{!!Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => 'true']) !!}
					<p class="help-block"></p>
					@if($errors->has('name'))
						<p class="help-block alert alert-danger">
							{{ $errors->first('name') }}
						</p>
					@endif

				</div>

				<div class="form-group required col-md-6">
					<label class="control-label" for="email">Email address</label>
					{!! Form::text('email', old('email'), ['class' => 'form-control', 'placeholder' => '', 'required' => 'true']) !!}
					<p class="help-block"></p>
					@if($errors->has('email'))
						<p class="help-block alert alert-danger">
							{!! $errors->first('email') !!}
						</p>
					@endif
				</div>

				<div class="form-group required col-md-6">
					<label class="control-label" for="phone">Contact Number</label>
					{!! Form::text('phone', old('phone'), ['class' => 'form-control', 'placeholder' => '', 'required' => 'true']) !!}
					<p class="help-block"></p>
					@if($errors->has('phone'))
						<p class="help-block alert alert-danger">
							{!! $errors->first('phone') !!}
						</p>
					@endif
				</div>

				<div class="form-group required col-md-6">
					<label class="control-label" for="country_id">Country</label>
					{!! Form::select('country_id', $country,  old('country_id'), ['class' => 'form-control', 'required' => 'true']) !!}
						<p class="help-block"></p>
					@if($errors->has('country_id'))
						<p class="help-block alert alert-danger">
							{{ $errors->first('country_id') }}
						</p>
					@endif

				</div>

				<div class="form-group required col-md-6">
					<label class="control-label" for="city">City</label>
					{!! Form::text('city', old('city'), ['class' => 'form-control', 'placeholder' => '', 'required' => 'true']) !!}
					<p class="help-block"></p>
					@if($errors->has('city'))
						<p class="help-block alert alert-danger">
							{{ $errors->first('city') }}
						</p>
					@endif

				</div>

				<div class="form-group required col-md-6">
					<label class="control-label" for="address">Current Location / Address</label>
					{!! Form::textarea('address', old('address'), ['class' => 'form-control', 'placeholder' => '', 'required' => 'true','rows' =>'3']) !!}
					<p class="help-block"></p>
					@if($errors->has('address'))
						<p class="help-block alert alert-danger">
							{{ $errors->first('address') }}
						</p>
					@endif

				</div>
				<div class="form-group required col-md-6">
					<label class="control-label" for="desc">Short Description</label>
					{!! Form::textarea('desc', old('desc'), ['class' => 'form-control', 'placeholder' => '', 'required' => 'true','rows' => '3']) !!}
					<p class="help-block"></p>
					@if($errors->has('desc'))
						<p class="help-block alert alert-danger">
							{{ $errors->first('desc') }}
						</p>
					@endif

				</div>

				<div class="form-group required col-md-12">
					<label class="control-label" for="service">List services that you would like to provide</label>
					{!! Form::textarea('service', old('service'), ['class' => 'form-control', 'placeholder' => '', 'required' => 'true','rows' => '3']) !!}
					<p class="help-block"></p>
					@if($errors->has('service'))
						<p class="help-block alert alert-danger">
							{{ $errors->first('service') }}
						</p>
					@endif

				</div>

					


			</div>
		</div>		
	</div>
</div>