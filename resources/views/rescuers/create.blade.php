@extends('base.layout')
@section('content')
<div class="container mt-2 mb-5">
{!! Form::open(['method' => 'POST', 'route' => ['rescuer.save'], 'files' => true,]) !!}
  
    
    
    <fieldset>
    <div class="alert alert-info" role="alert">

    <div class="container bg-light-blue floating">
		<div class="row">
			<div class="col-12 px-3 py-5 p-sm-4 px-lg-5 py-lg-4">
				<h3 class="font-w700 text-body">Want to become a rescuer?</h3>
                <p class="pb-3 pr-0 pr-sm-7">Please note that all fields are mandatory.</p>
				 
			</div>
		</div>
	</div>
    
    </div>
    
        @include('rescuers.form')
    
        <div class="col-md-12 mb-5">
        {!! Form::submit(trans('Submit'), ['class' => 'btn btn-lg btn-success']) !!}
        </fieldset>
        </div>
   
 
{!! Form::close() !!}
</div>
@endsection