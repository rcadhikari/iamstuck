@extends('base.layout')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
        <div class="alert alert-success" role="alert">
            @if(Session::has('message'))
                {!! Session::get('message') !!}
           @endif
        </div>
        </div>

        <div class="col-md-4">
            <div class="card">
            <img class="card-img-top" src="https://ichef.bbci.co.uk/news/660/cpsprodpb/10D2A/production/_111460986_2.53086737.jpg" alt="Card image cap">
            <div class="card-body">
            <p class="card-text">Coronavirus: Things will get worse, PM warns in letter to Britons - BBC </p>
            </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
            <img class="card-img-top" src="https://ichef.bbci.co.uk/news/660/cpsprodpb/C8B3/production/_111397315_gettyimages-1204793213.jpg" alt="Card image cap">
            <div class="card-body">
            <p class="card-text">Coronavirus: What we still don't know about Covid-19 -BBC</p>
            </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
            <img class="card-img-top" src="https://cdn.cnn.com/cnnnext/dam/assets/200328102757-01-spain-coronavirus-exlarge-169.jpg" alt="Card image cap">
            <div class="card-body">
            <p class="card-text">How Spain became a hotspot for coronavirus - CNN</p>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection