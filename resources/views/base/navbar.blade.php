<nav class="navbar navbar-expand-lg navbar-light bg-white">
    <div class="container">
        <div class="mr-auto">
            <a class="navbar-brand" href="{{route('home')}}"><img src="{{asset('assets/logo.png')}}" class="img-fluid header-image" alt="logo"></a>
        </div>

        <div class="d-md-none w-100 text-center">
                <a href="{{route('about')}}" class="link my-2">About</a>
        </div>

        <a href="{{route('about')}}" class="link my-2 mx-2 mx-md-4 mx-sm-0 d-none d-md-inline-block">About</a>
    
        @if (!(Request::is('report/submission')))
         <a href="{{route('report.submission')}}" class="btn btn-warning btn-lg my-2 my-sm-0">i am stuck</a>
         <a href="{{route('rescuer.register')}}" class="btn btn-success btn-lg my-2 my-sm-0 mx-3">Rescuers</a>
        @endif
        @if ((Request::is('report/submission')))
            <a href="{{route('home')}}" class="btn btn-success btn-lg my-2 my-sm-0 mx-3">Home</a>
        @endif
        

    </div>
   
</div>
</nav>
