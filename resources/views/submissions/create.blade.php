@extends('base.layout')
@section('content')
<div class="container mt-2 mb-5">
{!! Form::open(['method' => 'POST', 'route' => ['report.save'], 'files' => true,]) !!}
    <fieldset>
    <div class="alert alert-info" role="alert">

<div class="container bg-light-blue floating">
    <div class="row">
        <div class="col-12 px-3 py-5 p-sm-4 px-lg-5 py-lg-4">
            <h3 class="font-w700 text-body">Let's share the situation to find the help.</h3>
            <p class="pb-3 pr-0 pr-sm-7">Please note that all fields are mandatory.</p>
             
        </div>
    </div>
</div>

</div>
        {!! Form::open(['method' => 'POST', 'route' => ['report.save'], 'files' => true,]) !!}
        @include('submissions.form')

        <div class="col-md-12 mb-5">
        {!! Form::submit(trans('Submit'), ['class' => 'btn btn-lg btn-success']) !!}
    </fieldset>
</div>
    

{!! Form::close() !!}
</div>
@endsection
