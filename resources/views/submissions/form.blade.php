<div class="row mt-2">
<div class="col-12">
<div class="shadow p-3 mb-5 bg-white rounded">
<div class="row">
	<div class="col-12 text-center">
		<h2>Personal Information</h2>
 	</div>
</div>
<div class="row p-2">

<div class="form-group required col-md-6">
    <label class="control-label" for="firstname">First Name</label>
     {!!Form::text('firstname', old('firstname'), ['class' => 'form-control', 'placeholder' => '', 'required' => 'true']) !!}
    <p class="help-block"></p>
    @if($errors->has('firstname'))
        <p class="help-block alert alert-danger">
            {{ $errors->first('firstname') }}
        </p>
    @endif

</div>
<div class="form-group required col-md-6">
    <label class="control-label" for="lastname">Lastname</label>
    {!! Form::text('lastname', old('lastname'), ['class' => 'form-control', 'placeholder' => '', 'required' => 'true']) !!}
        <p class="help-block"></p>
    @if($errors->has('lastname'))
        <p class="help-block alert alert-danger">
            {{ $errors->first('lastname') }}
        </p>
    @endif

</div>
<div class="form-group required col-md-6">
    <label class="control-label" for="email">Email address</label>
    {!! Form::text('email', old('email'), ['class' => 'form-control', 'placeholder' => '', 'required' => 'true']) !!}
    <p class="help-block"></p>
    @if($errors->has('email'))
        <p class="help-block alert alert-danger">
            {!! $errors->first('email') !!}
        </p>
    @endif
</div>

<div class="form-group required col-md-6">
	<label class="control-label" for="mobile_phone">Mobile / Phone</label>
	{!! Form::text('mobile_phone', old('mobile_phone'), ['class' => 'form-control', 'placeholder' => '', 'required' => 'true']) !!}
	<p class="help-block"></p>
	@if($errors->has('mobile_phone'))
		<p class="help-block alert alert-danger">
			{!! $errors->first('mobile_phone') !!}
		</p>
	@endif
 </div>

 <div class="form-group required col-md-6">
    <label class="control-label" for="gender_id">Gender</label>
    {!! Form::select('gender_id', $gender,  old('gender_id'), ['class' => 'form-control', 'required' => 'true']) !!}
        <p class="help-block"></p>
    @if($errors->has('gender_id'))
        <p class="help-block alert alert-danger">
            {{ $errors->first('gender_id') }}
        </p>
    @endif

</div>

<div class="form-group required col-md-6">
    <label class="control-label" for="citizen_of">Citizenship</label>
    {!! Form::select('citizen_of', $country,  old('citizen_of'), ['class' => 'form-control', 'required' => 'true']) !!}
        <p class="help-block"></p>
    @if($errors->has('citizen_of'))
        <p class="help-block alert alert-danger">
            {{ $errors->first('citizen_of') }}
        </p>
    @endif

</div>

<div class="form-group required col-md-6">
<label class="control-label" for="permanent_address">Destination / Home address</label>
{!! Form::textarea('permanent_address', old('permanent_address'), ['class' => 'form-control', 'placeholder' => '', 'required' => 'true', 'rows' => '3']) !!}
	<p class="help-block"></p>
@if($errors->has('permanent_address'))
	<p class="help-block alert alert-danger">
		{{ $errors->first('permanent_address') }}
	</p>
@endif
</div>

</div>

</div>
</div>


<div class="col-12">
<div class="shadow p-3 mb-5 bg-white rounded">
<div class="row">
	<div class="col-12 text-center">
	 	<h2>Your Case Details</h2>
 	</div>
</div>
<div class="row p-2">

<div class="form-group required col-md-6">
	<label class="control-label" for="case_type_id">Case Type</label>
	{!! Form::select('case_type_id', $case_type,  old('case_type_id'), ['class' => 'form-control', 'required' => 'true']) !!}
	 <p class="help-block"></p>
	@if($errors->has('case_type_id'))
		<p class="help-block alert alert-danger">
			{{ $errors->first('case_type_id') }}
		</p>
	@endif
</div>

<div class="form-group required col-md-6">
    <label class="control-label" for="stuck_in_country">Which country are you in now?</label>
    {!! Form::select('stuck_in_country', $country,  old('stuck_in_country'), ['class' => 'form-control', 'required' => 'true']) !!}
        <p class="help-block"></p>
    @if($errors->has('stuck_in_country'))
        <p class="help-block alert alert-danger">
            {{ $errors->first('stuck_in_country') }}
        </p>
    @endif

</div>
<div class="form-group required col-md-6">
	<label class="control-label" for="city">City</label>
	{!! Form::text('city', old('city'), ['class' => 'form-control', 'placeholder' => '', 'required' => 'true']) !!}
	 <p class="help-block"></p>
	@if($errors->has('city'))
		<p class="help-block alert alert-danger">
			{{ $errors->first('city') }}
		</p>
	@endif

</div>
<div class="form-group required col-md-6">
	<label class="control-label" for="reason_id">Reason to visit this country</label>
	{!! Form::select('reason_id', $reason,  old('reason_id'), ['class' => 'form-control', 'required' => 'true']) !!}
	 <p class="help-block"></p>
	@if($errors->has('reason_id'))
		<p class="help-block alert alert-danger">
			{{ $errors->first('reason_id') }}
		</p>
	@endif

</div>


<div class="form-group required col-md-6">
	<label class="control-label" for="address">Current Location / Address</label>
	{!! Form::textarea('address', old('address'), ['class' => 'form-control', 'placeholder' => '', 'required' => 'true', 'rows' => '3']) !!}
	 <p class="help-block"></p>
	@if($errors->has('address'))
		<p class="help-block alert alert-danger">
			{{ $errors->first('address') }}
		</p>
	@endif

</div>




<div class="form-group required col-md-6">
	<label class="control-label" for="alone_group">Are you alone or with a group</label>
	{!! Form::select('alone_group', array('' => 'Select','Individual' => 'Individual','Group' => 'Group'),  old('alone_group'), ['id' =>'alone_group', 'class' => 'form-control', 'required' => 'true']) !!}
	 <p class="help-block"></p>
	@if($errors->has('alone_group'))
		<p class="help-block alert alert-danger">
			{{ $errors->first('alone_group') }}
		</p>
	@endif
</div>

 
<div class="form-group required col-md-6 group">
	<label class="control-label" for="members_above_or_16">Number of adults</label>
	{!! Form::select('members_above_or_16', $numbers,  old('members_above_or_16'), ['id' =>'members_above_or_16', 'class' => 'form-control', 'required' => 'true']) !!}
	 <p class="help-block"></p>
	@if($errors->has('members_above_or_16'))
		<p class="help-block alert alert-danger">
			{{ $errors->first('members_above_or_16') }}
		</p>
	@endif
</div>

<div class="form-group required col-md-6 group">
	<label class="control-label" for="members_under_16">Number of people age under 16 years</label>
	{!! Form::select('members_under_16', $numbers,  old('members_under_16'), ['id' =>'members_under_16', 'class' => 'form-control', 'required' => 'true']) !!}
	 <p class="help-block"></p>
	@if($errors->has('members_under_16'))
		<p class="help-block alert alert-danger">
			{{ $errors->first('members_under_16') }}
		</p>
	@endif
</div>
 


<div class="form-group required col-md-12">
	<label class="control-label" for="explain_situation">Explain your current situation</label>
	{!! Form::textarea('explain_situation', old('explain_situation'), ['class' => 'form-control', 'placeholder' => '', 'required' => 'true', 'rows' => '5']) !!}
	 <p class="help-block"></p>
	@if($errors->has('explain_situation'))
		<p class="help-block alert alert-danger">
			{{ $errors->first('explain_situation') }}
		</p>
	@endif

</div>
</div>
</div>
</div>
</div>
