@extends('base.layout')

@section('content')

<div class="container mt-5">
    <div class="mb-5 row text-center">
        <div class="col-12 ">
            <h1 class="header-number headers-text text-danger mb-0">
                <stong><span class="count">{{ $total->stranded_people + $total->rescued_people }}</span></stong>
            </h1> <br/>
            <h1 class="headers-text light-grey mt-0">Total People Stuck</h1>
        </div>
    </div>

    <div class="mb-5 mt-5 row">
        <div class="col-md-4 text-center">
            <h1 class="header-number headers-text text-success mb-0">
                <stong><span class="count">{{ $total->rescued_people }}</span></stong>
            </h1>  <br/>
            <h1 class="subheading-text light-grey mt-0">
                Rescued in {{ $total->total_countries_rescued }} {{ ($total->total_countries_rescued > 1 ) ? 'countries' : 'country' }}
            </h1>
        </div>
        <div class="col-md-4 text-center">
            <h1 class="header-number headers-text text-orange mb-0">
            <stong><span class="count">{{ $total->stranded_people }}</span></stong></h1>  <br/>
            <h1 class="subheading-text light-grey mt-0">
                Stuck in {{ $total->total_countries_stranded }} {{ ($total->total_countries_stranded > 1) ? 'countries' : 'country'  }}
            </h1>
        </div>
        <div class="col-md-4 text-center">
            <h1 class="header-number headers-text text-info mb-0">
                <stong><span class="count">{{ $total->total_rescuers }}</span></stong>
            </h1>  <br/>
            <h1 class="subheading-text light-grey mt-0">
                Rescuers in {{ $total->total_rescuers_countries }} {{ ($total->total_rescuers_countries > 1 ) ? 'countries' : 'country' }}
            </h1>
        </div>
    </div>

   

    <div class="mb-5 Justify-content-center">

    <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">

        <thead>
            <tr>
                <th class="all">Country</th>
                <th class="desktop tablet-p tablet-l">Stranded People</th>
                <th class="desktop tablet-p tablet-l">Stranded Foreigners</th>
                <th class="desktop tablet-p tablet-l">Rescuers</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($reports as $row)
            <tr>
                <td><a href="{{ route('report.detail', $row->id) }}">{{ $row->country }} ({{ $row->country_code }})</a></td>
                <td>{{ $row->nationals ?? '-' }}</td>
                <td>{{ $row->foreigners ?? '-' }}</td>
                <td>{{ $row->rescuers ?? '-' }}</td>
            </tr>
        @endforeach

    </table>
    </div>

</div>

@endsection
