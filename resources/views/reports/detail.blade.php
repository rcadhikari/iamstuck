@extends('base.layout')

@section('content')

<div class="container mt-5">
	<div class="row mt-2">
		<div class="col-12">
			<div class="shadow p-3 mb-5 bg-white rounded">
				<div class="row">
					<div class="col-12 text-left">
					<div class="clearfix float-right">
							<a class="btn btn-info text-white" href="{{route('home')}}">Go Back</a>
						</div>	
						<div class="title">
								<strong>Report of  {{ $info->nicename }}</strong> &nbsp;
								<img src="https://www.countryflags.io/{{ strtolower($info->iso) }}/flat/32.png">
						</div>	

						

						<ul class="nav nav-tabs" id="myTab" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Nationals</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Foreigners</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Rescuers</a>
							</li>
							</ul>

					</div>
				</div>

			<div class="tab-content" id="myTabContent">
			
			<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
				@include('reports.blog', [
							'summary' => $nationals['summary'],
							'reports' => $nationals['reports'],
							'title' => 'Stranded Nationals',
							'link_destination_country' => false,
							'link_stuck_in_country' => true,
						])
			</div>
			
			<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
				@include('reports.blog', [
							'summary' => $foreigners['summary'],
							'reports' => $foreigners['reports'],
							'title' => 'Stranded Foreigners',
							'link_destination_country' => true,
							'link_stuck_in_country' => false,
						])
			</div>
			
			<div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
				@include('reports.blog-rescuer', [
							'summary' => $rescuers['summary'],
							'reports' => $rescuers['reports'],
							'title' => 'Nationwide Rescuers',
							'link_country' => true
						])
			</div>
				
				</div>
			</div>
		</div>
	</div>
 



@endsection
