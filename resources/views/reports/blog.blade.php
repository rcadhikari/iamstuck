<div class="row mt-5 display">
	@if($summary->total_submissions)
		<div class="col-md-12">
			<h4>{{ $title }}</h4>
			<div>Submissions: <strong>{{ $summary->total_submissions }}</strong> <br/></div>
			<div>People: <strong>{{ $summary->total_people }}</strong></div>
		</div>
	@else
		<div class="col-md-12">No records found.</div>
	@endif
</div>

@foreach ($reports as $row)
<div class="row mt-2 mb-5 display">
<div class="col-md-6">
		{{ $loop->index + 1 }}.
		<strong>{{ $row->firstname . ' ' . $row->lastname }} ({{$row->gender->name}}) </strong>	
		<div class="date">Submission Date: {{ date('h:i:s a, j F, Y', strtotime($row->created_at)) }}</div>
		<div>
			Citizen of: {{ $row->permanent_address }},
			@if ($link_destination_country)
				<a href="{{ route('report.detail', $row->citizenOf->id) }}">{{ $row->citizenOf->nicename }}</a>
				<img src="https://www.countryflags.io/{{ strtolower($row->citizenOf->iso) }}/flat/16.png">
			@else
				{{ $row->citizenOf->nicename }}
			@endif
		</div>
		<div>
			@if( strtolower($row->alone_group) == 'group')
				Total <strong>{{ $row->members_above_or_16 + $row->members_under_16 }}</strong>
				{{ $row->members_under_16 > 0 ? '(' . $row->members_under_16 . ' under age 16)' : '' }} people
			@endif
		</div>
		<div>

		<strong>Stuck in </strong> {{ $row->address }}, {{ $row->city }},
		@if ($link_stuck_in_country)
			<a href="{{ route('report.detail', $row->stuckInCountry->id) }}">{{ $row->stuckInCountry->nicename }}</a>
			<img src="https://www.countryflags.io/{{ strtolower($row->stuckInCountry->iso) }}/flat/16.png">
		@else
			{{ $row->stuckInCountry->nicename }}
		@endif
	</div>
</div>
<div class="col-md-4 mt-2 mt-md-0">
<strong>Current Situation:</strong> <br/>
		{{ $row->explain_situation }}
</div>

<div class="col-md-2">
	<a type="button" class="btn btn-success btn-sm text-white" data-toggle="modal" data-target="#m{{$row->id}}">Contribute</a>
</div>

@include('rescues.modal')

</div>
@endforeach








<!--


<table id="articles" class="mt-5 display" style="width:100%">
	<thead>
		<h4 class="mt-5">{{ $title }}</h4>
		<div>
			<h5>{{ $title }}</h5>
			Submissions: <strong>{{ $summary->total_submissions }}</strong> <br/>
			People: <strong>{{ $summary->total_people }}</strong>
		</div>
	</thead>

	<tbody>
	@foreach ($reports as $row)
		<tr id="article">
			<td valign="top">{{ $loop->index + 1 }}.</td>
			<td>
				<div>
					<a href="{{ route('report.detail', $row->id) }}">
						{{ $row->firstname . ' ' . $row->lastname }} ({{$row->gender->name}})
					</a>
				</div>
				<div class="date"> {{ date('h:i:s a, j F, Y', strtotime($row->created_at)) }}</div>
				<div>
					Destination: {{ $row->permanent_address }},
					@if ($link_destination_country)
						<a href="{{ route('report.detail', $row->citizenOf->id) }}">{{ $row->citizenOf->nicename }}</a>
						<img src="https://www.countryflags.io/{{ strtolower($row->citizenOf->iso) }}/flat/16.png">
					@else
						{{ $row->citizenOf->nicename }}
					@endif
				</div>
				<div>
					@if( strtolower($row->alone_group) == 'group')
						Total: <strong>{{ $row->members_above_or_16 + $row->members_under_16 }}</strong>
						{{ $row->members_under_16 > 0 ? '(' . $row->members_under_16 . ' under age 16)' : '' }} people
					@endif
					<strong>stuck in </strong> {{ $row->address }}, {{ $row->city }},

					@if ($link_stuck_in_country)
						<a href="{{ route('report.detail', $row->stuckInCountry->id) }}">{{ $row->stuckInCountry->nicename }}</a>
						<img src="https://www.countryflags.io/{{ strtolower($row->stuckInCountry->iso) }}/flat/16.png">
					@else
						{{ $row->stuckInCountry->nicename }}
					@endif

				</div>
				<divp> <strong>Current Situation:</strong> <br/>
					{{ $row->explain_situation }}
				</divp>
				<br/><br/>
			</td>
			<td>
			<strong>Current Situation:</strong> <br/>
					{{ $row->explain_situation }}
			</td>
		</tr>
	@endforeach
	</tbody>

</table>
-->
