<div class="row mt-5 display">
	@if($summary->total_rescuers)
	<div class="col-md-12">
		<h4>{{ $title }}</h4>
		<div>Total: <strong>{{ $summary->total_rescuers }}</strong></div>
	</div>
	@else
		<div class="col-md-12">No rescuer available.</div>
	@endif
</div>

@foreach ($reports as $row)
<div class="row mt-2 mb-5 display">
	<div class="col-md-6">
		{{ $loop->index + 1 }}.
		<strong>{{ $row->name }} ({{ $row->rescuerType->name }}) </strong>
		<div class="date">Register On: {{ date('h:i:s a, j F, Y', strtotime($row->created_at)) }}</div>
		<div>
			Address: {{ $row->address }}, {{ $row->city }}
			@if ($link_country)
				<a href="{{ route('report.detail', $row->country->id) }}">{{ $row->country->nicename }}</a>
				<img src="https://www.countryflags.io/{{ strtolower($row->country->iso) }}/flat/16.png">
			@else
				{{ $row->country->nicename }}
			@endif
			<br/>
			Contact: {{ $row->phone }}, {{ $row->email }}
		</div>
	</div>

	@if (!empty($row->desc))
	<div class="col-md-4 mt-2 mt-md-0">
		<strong>Info:</strong> <br/>
			{{ $row->desc }}
	</div>
	@endif

	@if (!empty($row->services))
	<div class="col-md-4 mt-2 mt-md-0">
		<strong>Services:</strong> <br/>
		{{ $row->services }}
	</div>
	@endif

</div>
@endforeach
