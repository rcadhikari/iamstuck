$(document).ready(function() {
    $('#example').DataTable({
        "order": [[ 1, "ASC" ]]
    });
});


$(function() {
 
    $( "#alone_group" ).change(function() {

    var conceptName = $('#alone_group').find(":selected").text();
    console.log(conceptName);

    if (conceptName == "Alone") {
        $('.group').hide();
        }
      else if (conceptName == "Group") {
        $('.group').show();
        }

    });

    $('.count').each(function () {
      $(this).prop('Counter',0).animate({
          Counter: $(this).text()
      }, {
          duration: 3000,
          easing: 'swing',
          step: function (now) {
              $(this).text(Math.ceil(now));
          }
      });
  });


  });
  