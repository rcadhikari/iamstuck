<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@home')->name('home');
Route::get('about','HomeController@about')->name('about');

# Report API
Route::prefix('report')->group( function() {
    Route::get('submission', 'Reports\ReportController@create')->name('report.submission');
    Route::post('save', 'Reports\ReportController@store')->name('report.save');
    Route::get('submitted', 'Reports\ReportController@submitted')->name('report.submitted');
    Route::get('detail/{countryId}', 'Reports\ReportController@show')->name('report.detail');
});

Route::prefix('rescue')->group( function() {
    Route::get('register', 'Rescues\RescuerController@create')->name('rescuer.register');
    Route::post('save', 'Rescues\RescuerController@store')->name('rescuer.save');
    Route::get('submitted', 'Rescues\RescuerController@submitted')->name('rescuer.submitted');
   // Route::get('detail', 'Rescues\ControllerRescuer@create')->name('rescuer.detail');
   Route::post('save-rescue', 'Rescues\RescueController@store')->name('save-rescue');
});

